import React, { Component, PropTypes } from 'react'

import { Navbar, Nav, NavItem, FormGroup } from 'react-bootstrap'
// import Autosuggest from 'react-autosuggest'
import Style from 'style-it'
import { FaPowerOff, FaUser, FaPlus } from 'react-icons/lib/fa'

class NavHeader extends Component {

  static propTypes = { children: PropTypes.node }
  static contextTypes = { flux: PropTypes.object.isRequired }

  state = { i18n: this.context
      .flux.getStore('locale').getState() }

  componentDidMount() {
    const { flux } = this.context
    flux.getStore('helmet').listen(this.handleTitleChange)
  }

  componentWillUnmount() {
    const { flux } = this.context
    flux.getStore('helmet').unlisten(this.handleTitleChange)
  }

  handleTitleChange({ titleBase, title }) {
    document.title = titleBase + title
  }

  render() {
    // let { user } = this.props.viewer;
    const isAnon = true // (user.role=='AnonymousUser');
    let logoutOrLoginSection = []

    // this.state.suggestions = this.getSuggestions(this.state.value)

    // const { value, suggestions } = this.state
    // const search = this.search

    // Autosuggest will pass through all these props to the input element.
    // const inputProps = {
      // placeholder: 'Search'
      // value,
      // onChange: this.onChange,
      // onKeyDown: this.onKeyDown
    // }

    if (isAnon) {
      logoutOrLoginSection = [
        <Style>
          {`
            .navbar-nav>li>a:hover {
              background-color: #017175
            }
            .navbar-button.active {
              background-color: #017175
            }
          `}

          <NavItem className='navbar-button'>
            <div style={ { color: '#b5fed9' } }>
              <FaPowerOff /> Login
            </div>
          </NavItem>
        </Style>
      ]
    } else {
      logoutOrLoginSection = [
        <Style>
          {`
            .navbar-nav>li>a:hover {
              background-color: #017175
            }
            .navbar-button.active {
              background-color: #017175
            }
            .navbar-nav>.active>a:hover {
              background-color: #017175
            }
          `}

          <NavItem className='navbar-button' href={ '/profile/' } active={ false }>
            <div style={ { color: '#b5fed9' } }>
              <FaUser />
              { 'Welcome Sid!' }
            </div>
          </NavItem>
        </Style>
      ]
    }

    const css = `
                @media (max-width: 767px) {
                  .main-nav-header .navbar-form {
                     margin: 0;
                     float: right;
                  }
                }
              `

    const postButtonCss = `
      .post-button:hover {
        background-color: #353f48;
        border-color: #313a42
      }
      .post-button {
        background-color: #38434c;
        border-color: #313a42
      }
    `


    return (
      <Navbar fixedTop={ true } style={ { marginBottom: '0px', backgroundColor: '#006064!important', borderRadius: '0px!important' } }>
        <Navbar.Header className='main-nav-header'>
          <Navbar.Brand>
            <a href='/' style={ { color: '#b5fed9' } }>Merikos</a>
          </Navbar.Brand>
          <style dangerouslySetInnerHTML={ { __html: css } } />
          <Navbar.Form pullLeft={ true } className='col-xs-6' style={ { border: 0 } }>
            <FormGroup />
          </Navbar.Form>
        </Navbar.Header>

        <Navbar.Collapse>
          <Nav pullRight={ true }>
            <style dangerouslySetInnerHTML={ { __html: postButtonCss } } />
            <button className='btn btn-primary navbar-btn post-button' style={ { color: '#b5fed9' } } onClick={ this.postContent }><FaPlus /> POST</button>
            {
              // <Style>
              //   {`
              //     .navbar-nav>li>a:hover {
              //       background-color: #017175
              //     }
              //     .navbar-button.active {
              //       background-color: #017175
              //     }
              //   `}
              //   <NotificationList />
              // </Style>
            }
            <Style>
              {`
                .navbar-nav>li>a:hover {
                  background-color: #017175
                }
                .navbar-button.active {
                  background-color: #017175
                }
              `}

              <NavItem className='navbar-button' href={ 'https://medium.com/humbee-blog' }>
                <div style={ { color: '#b5fed9' } }>
                  Blog
                </div>
              </NavItem>
            </Style>
            <Style>
              {`
                .navbar-nav>li>a:hover {
                  background-color: #017175
                }
                .navbar-button.active {
                  background-color: #017175
                }
                .navbar-nav>.active>a:hover {
                  background-color: #017175
                }
                .navbar-nav>.active>a {
                  background-color: #017175
                }
                .navbar-default {
                  background-color: #017175
                }
              `}

              <NavItem className='navbar-button' href={ '/community/-1' } active={ false }>
                <div style={ { color: '#b5fed9' } }>
                  Originals
                </div>
              </NavItem>
            </Style>


            { logoutOrLoginSection }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }

}

export default NavHeader
