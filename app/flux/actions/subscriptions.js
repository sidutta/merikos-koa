class SubscriptionsActions {

  constructor() {
    this.generateActions(
      'searchSuccess',
      'searchFail'
    )
  }

  search(text) {
    return (dispatch, alt) =>
      alt.resolve(async () => {
        try {
          alt.getActions('requests').start()
          const response = await alt.request({ url: `/search/${text}` })
          console.log('response: ')
          console.log(response)
          this.searchSuccess(response)
        } catch (error) {
          console.log('error: ')
          console.log(error)
          this.searchFail({ error })
        }
        alt.getActions('requests').stop()
      })
  }

}

export default SubscriptionsActions
