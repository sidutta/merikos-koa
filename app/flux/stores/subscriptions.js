class SubscriptionsStore {

  constructor() {
    this.bindActions(this.alt.getActions('subscriptions'))

    this.collection = []
    this.error = null
  }

  onSearchSuccess(subscriptions) {
    this.collection = subscriptions
    this.error = null
    console.log('collection is:')
    console.log(this.collection)
  }

  onSearchFail({ error }) {
    this.error = error
  }

}

export default SubscriptionsStore
