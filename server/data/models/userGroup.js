export default (sequelize, DataTypes) => {
  var userGroup = sequelize.define('UserGroup', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    targetSize: {
      type: DataTypes.INTEGER,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'UserGroupType';
      }
    },
  });
  userGroup.associate = models => {
    userGroup.belongsToMany(models.User, {through: models.UserGroupToUserAssociation});
    userGroup.belongsToMany(models.PricingList, {through: models.UserGroupToPricingListAssociation});
  }
  return userGroup;
};
