export default (sequelize, DataTypes) => {
  var pricingList = sequelize.define('PricingList', {
    price: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    duration: {
      type: DataTypes.ENUM('Day', 'Week', 'Month', 'Year'),
      allowNull: false
    },
    currency: {
      type: DataTypes.ENUM('INR', 'USD', 'EURO', 'JPY', 'GBP', 'RMB', 'HKD', 'CHF', 'AUD', 'CAD', 'SGD'),
      allowNull: false
    },
    merikos_commission: {
      type: DataTypes.FLOAT,
      allowNull : true
    },
    minUsers : {
      type : DataTypes.INTEGER,
      allowNull : true
    },
    maxUsers : {
      type : DataTypes.INTEGER,
      allowNull : true
    },
    active : {
      type : DataTypes.BOOLEAN,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'PricingListType';
      }
    },
  });
  pricingList.associate = models => {
    pricingList.belongsTo(models.Subscription, {
      onDelete : 'cascade',
      foreignKey : {
        allowNull : false
      }
    });
    pricingList.belongsToMany(models.UserGroup, {through: models.UserGroupToPricingListAssociation});

  }
  return pricingList;
};
