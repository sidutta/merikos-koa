import React, { Component, PropTypes } from 'react'
import connect from 'connect-alt'
import { Link } from 'react-router'
import Autosuggest from 'react-autosuggest'

import imageResolver from 'utils/image-resolver'
// import Spinner from 'components/shared/spinner'
// import LangPicker from 'components/shared/lang-picker'

// // Load styles for the header
// // and load the `react-logo.png` image
// // for the `<img src='' />` element
let reactLogo
/* istanbul ignore next */
if (process.env.BROWSER) {
  reactLogo = require('images/react-logo.png')
} else {
  reactLogo = imageResolver('images/react-logo.png')
}

const theme = {
  container: {
    position: 'relative',
    width: '350px',
    margin: '0 auto'
  },
  input: {
    width: '100%', // '240px',
    height: '33px',
    padding: '5px 20px 2px 40px',
    fontFamily: 'Helvetica, sans-serif',
    fontWeight: 300,
    fontSize: '16px',
    border: '1px solid #aaa',
    borderRadius: '4px'
  },
  inputFocused: {
    outline: 'none'
  },
  inputOpen: {
    'border-bottom-left-radius': 0,
    'border-bottom-right-radius': 0
  },
  suggestionsContainer: {
    display: 'none'
  },
  suggestionsContainerOpen: {
    display: 'block',
    position: 'absolute',
    top: '34px',
    border: '1px solid #aaa',
    backgroundColor: '#fff',
    fontFamily: 'Helvetica, sans-serif',
    fontWeight: 300,
    fontSize: '16px',
    'border-bottom-left-radius': '4px',
    'border-bottom-right-radius': '4px',
    'z-index': 2
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    'list-style-type': 'none'
  },
  suggestion: {
    cursor: 'pointer',
    padding: '10px 20px'
  },
  suggestionHighlighted: {
    backgroundColor: '#ddd'
  },
  inputContainer: {
    position: 'relative'
  },
  icon: {
    position: 'absolute',
    top: '5px',
    left: '10px',
    width: '24px',
    height: '24px',
    color: '#7ba098'
  }
}

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = (suggestion) => suggestion.name

// Use your imagination to render suggestions.
const renderSuggestion = (suggestion) => (
  <div>
    {suggestion.name}
  </div>
)

// @connect(({ requests: { inProgress }, session: { session } }) =>
  // ({ inProgress, session }))
@connect(({ subscriptions: { collection } }) => ({ collection }))
class Header extends Component {

  constructor() {
    super()

    this.state = {
      value: '',
      suggestions: []
    }
  }

  static contextTypes = {
  //   locales: PropTypes.array.isRequired,
    flux: PropTypes.object.isRequired
    // i18n: PropTypes.func.isRequired
  }

  props: {
  //   inProgress: boolean;
    // session: ?Object;
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    })
  }

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    const { flux } = this.context
    flux.getActions('subscriptions').search(value)
  }

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  // handleLocaleChange = (locale: string) => {
  //   const { flux } = this.context
  //   flux.getActions('locale').switchLocale({ locale })
  // }

  // handleLogout = () => {
  //   const { flux } = this.context
  //   flux.getActions('session').logout()
  // }

  componentWillReceiveProps({ collection }) {
    this.setState({
      suggestions: collection
    })
  }

  render() {
    // const { session } = this.props
    // const { inProgress, session } = this.props
    // const { locales: [ activeLocale ], i18n } = this.context
    // const { i18n } = this.context
    const { value, suggestions } = this.state

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Type a programming language',
      value,
      onChange: this.onChange
    }

    return (
      <header className='app--header' style={ { height: '233px', backgroundImage: 'url("https://res.cloudinary.com/urbanclap/image/upload/fl_progressive:steep/v1490770948/2x_1.jpg")', backgroundSize: 'cover' } }>
        <div style={ { position: 'relative', height: '233px', backgroundColor: 'rgba(77,77,77,.5)' } } />
        <div style={ { position: 'absolute', zIndex: 10, top: '0%', margin: '0 auto', width: '100%' } }>
          <Link to='/' className='app--logo'>
            <img src={ reactLogo } alt='react-logo' />
          </Link>
        </div>
        <div style={ { position: 'absolute', zIndex: 10, top: '80%', margin: '0 auto', width: '100%' } }>
          <Autosuggest
            suggestions={ suggestions }
            onSuggestionsFetchRequested={ this.onSuggestionsFetchRequested }
            onSuggestionsClearRequested={ this.onSuggestionsClearRequested }
            getSuggestionValue={ getSuggestionValue }
            renderSuggestion={ renderSuggestion }
            inputProps={ inputProps }
            theme={ theme }
            alwaysRenderSuggestions={ true } />
        </div>
        { /* Spinner in the top right corner
        <Spinner active={ inProgress } />

         LangPicker on the right side
        <LangPicker
          activeLocale={ activeLocale }
          onChange={ this.handleLocaleChange } />

         React Logo in header
        <Link to='/' className='app--logo'>
          <img src={ reactLogo } alt='react-logo' />
        </Link>

        { /* Links in the navbar
        <ul className='app--navbar text-center reset-list un-select'>
          <li>
            <Link to={ i18n('routes.users') }>
              { i18n('header.users') }
            </Link>
          </li>
          <li>
            <Link to={ i18n('routes.guides') }>
              { i18n('header.guides') }
            </Link>
          </li>
          { session ?
          [
            <li key={ 0 }>
              <Link to={ i18n('routes.account') }>
                { i18n('header.account') }
              </Link>
            </li>,
            <li key={ 1 }>
              <a href='/' onClick={ this.handleLogout }>
                { i18n('header.logout') }
              </a>
            </li>
          ] :
          <li>
            <Link to={ i18n('routes.login') }>
              { i18n('header.login') }
            </Link>
          </li>
          }
        </ul>
        */ }
      </header>
    )
  }
}

export default Header
