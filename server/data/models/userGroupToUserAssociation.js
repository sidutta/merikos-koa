export default (sequelize, DataTypes) => {
  var userGroupToUserAssociation = sequelize.define('UserGroupToUserAssociation', {
    UserId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    UserGroupId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'UserGroupToUserAssociation';
      }
    },
  });
  return userGroupToUserAssociation;
};
