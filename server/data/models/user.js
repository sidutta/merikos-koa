export default (sequelize, DataTypes) => {
  var user = sequelize.define('User', {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
      },
      allowNull : true,
      unique: {
        args: true,
        message: 'Email must be unique.'
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull : false
    },
    profilePic : {
      type : DataTypes.STRING,
      allowNull : true
    },
    phoneNumber : {
      type : DataTypes.STRING,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'UserType';
      }
    },
  });
  user.associate = models => {
    user.hasMany(models.SocialProfile);
    user.belongsToMany(models.UserGroup, {through: models.UserGroupToUserAssociation});
  }
  return user;
};
