export default (sequelize, DataTypes) => {
  var userGroupToPricingListAssociation = sequelize.define('UserGroupToPricingListAssociation', {
    UserGroupId: {
      type: DataTypes.INTEGER,
      allowNull : false
    },
    PricingListId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'UserGroupToPricingListAssociation';
      }
    },
  });
  return userGroupToPricingListAssociation;
};
