export default (sequelize, DataTypes) => {
  var subscription = sequelize.define('Subscription', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rank: {
      type: DataTypes.INTEGER,
      allowNull : true
    },
    categoryArray : {
      type : DataTypes.ENUM('Shopping', 'Apparel', 'Clothing Accessories'),
      allowNull : true
    },
    categoryProbabilityArray : {
      type : DataTypes.ARRAY(DataTypes.DECIMAL),
      allowNull : true
    },
    active : {
      type : DataTypes.BOOLEAN,
      allowNull : true
    },
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'SubscriptionType';
      }
    },
  });
  subscription.associate = models => {
    subscription.hasMany(models.PricingList);
  }
  subscription.search = query => {
    return sequelize.query('SELECT * FROM "Subscriptions" WHERE to_tsvector(\'english\', coalesce(name,\'\')) @@ to_tsquery(\'english\', ' + (query) + ')', this);
  }
  return subscription;
};
